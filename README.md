# Lab-4-Task-Squirrel
Lab 4 assignment from CodePath's Intermediate-iOS-Development-Spring-2023 course.

## Overview
You will be completing TaskSquirrel, which is an app that desperate parents use to get their children to do chores and help them out. This app has a list of tasks for the user to complete. Users will be required to attach photos to show that they have completed the task. After attaching the photo to a task, the app shows where that photo was taken in a map

![lab-4](https://user-images.githubusercontent.com/5723692/226035020-c2d54a2c-9a15-42ef-baa0-4c4e9566a911.gif)
